
#include "daq_tokens/acquire.h"

#include <iostream>

int main()
{
    try {
        std::string token = daq::tokens::acquire();
        std::cout << token;
    } catch(ers::Issue& ex) {
        std::cerr << ex << std::endl;
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
    }
}
