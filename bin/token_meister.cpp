#include "gssapi-utils/gssapi.h"

#include <jwt-cpp/traits/nlohmann-json/traits.h>
#include <jwt-cpp/jwt.h>

#include <boost/asio.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <openssl/crypto.h>
#include <openssl/evp.h>

#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <chrono>
#include <memory>
#include <atomic>

#include <pwd.h>
#include <csignal>

typedef std::string private_key_t;

char hostname[HOST_NAME_MAX] = "";

// Create a SHA256 hash of the public key to be used as key identifer
std::string
get_fingerprint(const std::string& key, const std::string& digest)
{
    auto evp_priv = jwt::helper::load_private_key_from_string(key);

    unsigned char *data = nullptr;
    int size = i2d_PUBKEY(evp_priv.get(), &data);
    if(size < 0) { throw std::runtime_error("Cannot convert to DER"); }

    std::unique_ptr<unsigned char[]> holder(data);

    // Create digest context, CentOS 8 has newer openssl version.
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_destroy)> ctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#else
    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)> ctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#endif

    if(!ctx) { throw std::runtime_error("Cannot create MD context"); }

    if(!EVP_DigestInit(ctx.get(), EVP_get_digestbyname(digest.c_str()))) { throw std::runtime_error("Cannot initialize message digest"); }

    if(!EVP_DigestUpdate(ctx.get(), data, size)) { throw std::runtime_error("Cannot update message digest"); }

    std::unique_ptr<uint8_t[]> fingerprint(new uint8_t[EVP_MD_CTX_size(ctx.get())]);
    unsigned int length;

    if(!EVP_DigestFinal(ctx.get(), fingerprint.get(), &length)) {
        throw std::runtime_error("Cannot finalize message digest");
    }

    // Turn into hex
    // char fp[130];
    std::unique_ptr<char[]> out(new char[length * 2 + 1]);
    auto fp = out.get();
    for(decltype(length) i = 0; i < length; i++) {
        sprintf(&fp[i*2], "%02x", fingerprint[i]);
    }
    fp[length * 2] = '\0';
    return fp;
}

// Create the token from the information given
std::string
make_token(const private_key_t& key, const std::string& fp, const std::string& user, const std::string& hostname)
{
    auto uuid{boost::uuids::random_generator()()};

    auto now = std::chrono::system_clock::now();
    auto token = jwt::create()
        .set_type("JWT")
        .set_issuer("https://auth.cern.ch/auth/realms/cern")
        .set_audience("atlas-tdaq-token")
        .set_issued_at(now)
        .set_expires_at(now + std::chrono::minutes{20})
        .set_not_before(now)
        .set_subject(user)
        .set_id(boost::uuids::to_string(uuid))
        .set_key_id(fp)
        .set_payload_claim("host", jwt::claim(hostname))
        .sign(jwt::algorithm::rs256{"",key});
    return token;
}

void
serve_local(const private_key_t& key, const std::string& fp, const std::string& socket_path)
{
    if(socket_path.size() > 107) {
        std::cerr << "token_meister: socket path too long (max 107 chars)\n";
        exit(1);
    }

    using namespace boost::asio::local;

    boost::asio::io_context ctx;
    stream_protocol::acceptor server_sock(ctx);

    try {
        if(getenv("LISTEN_FDS")) {
            // systemd socket activation
            server_sock = std::move(stream_protocol::acceptor(ctx, stream_protocol(), 3));
        } else {
            remove(socket_path.c_str());
            server_sock.open();
            server_sock.bind(stream_protocol::endpoint(socket_path));
            server_sock.listen(SOMAXCONN);
        }
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        exit(1);
    }

    while(true) {
        try {
            stream_protocol::socket client(ctx);
            server_sock.accept(client);

            ucred credentials;
            socklen_t length = sizeof credentials;
            if(getsockopt(client.native_handle(), SOL_SOCKET, SO_PEERCRED, &credentials, &length) < 0) {
                perror("token_meister: getsockopt(SO_PEERCRED)");
                exit(1);
            }

            if(auto pw = getpwuid(credentials.uid)) {
                auto token = make_token(key, fp, pw->pw_name, hostname);
                boost::asio::write(client, boost::asio::buffer(token.c_str(), token.size()));
            } else {
                std::cerr << "token_meister: no credentials\n";
            }
        } catch(std::exception& ex) {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void
serve_gssapi(const private_key_t& key, const std::string& fp, const std::string& port)
{
    using namespace gssapi_utils::net;

    io_context ctx;
    ip::tcp::acceptor server_socket(ctx);

    if(getenv("LISTEN_FDS")) {
        // systemd socket activation
        server_socket = ip::tcp::acceptor(ctx, ip::tcp::v6(), 3);
    } else {
        server_socket.open(ip::tcp::v6());

        server_socket.set_option(ip::v6_only(false));
        server_socket.set_option(socket_base::reuse_address(true));

        server_socket.bind(ip::tcp::endpoint(ip::tcp::v6(),
                                             (unsigned short)strtoul(port.c_str(), nullptr, 10)));
        server_socket.listen(SOMAXCONN);
    }

    while(true) {
        try {
            std::string user;
            auto sock = server_socket.accept();
            sock.set_option(ip::tcp::no_delay(true));
            auto ep   = sock.remote_endpoint();
            ip::tcp::resolver resolve(ctx);
            auto it = resolve.resolve(ep);
            gssapi_utils::context context{std::move(sock), user, "atdaqjwt"};
            std::string token = make_token(key, fp, user.substr(0, user.find('@')), it->host_name());
            context.send(token.c_str(), token.size());
        } catch(std::exception& ex) {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void time_it(const private_key_t& key, const std::string& fp, const std::string& user)
{
    const int count = 1000;

    auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < count; i++) {
        [[maybe_unused ]] auto token = make_token(key, fp, user, hostname);
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> time = end - start;
    std::cout << time.count()/count << " milliseconds/token" << std::endl;
}

std::string
load_private_key(const std::string& path)
{
    std::ifstream s{path.c_str()};
    if(s) {
        char buffer[32000];
        s.read(buffer, sizeof buffer - 1);
        buffer[s.gcount()] = '\0';
        return buffer;
    }
    return "";
}

void usage()
{
    std::cerr
        << "usage: token_meister [--gssapi|--make|--time][--hash=...] /path/to/private/key /path/to/socket|port|user\n\n"
        << "   --local       run a local server provding tokens on /path/to/socket (DEFAULT)\n"
        << "   --gssapi      runs a GSSAPI server listening on TCP 'port'\n"
        << "   --make        generates a token for 'user' interactively\n"
        << "   --time        timing output to generate token for 'user'\n"
        << "   --hash=<HASH> select hash function for finger print\n"
        << std::endl;
}

int
main(int argc, char *argv[])
{
    enum Mode { local, gssapi, make, timing } mode{local};

    if(argc < 2) {
        usage();
        exit(1);
    }

    argc--;
    argv++;

    OpenSSL_add_all_digests();
    std::string digest{"SHA256"};

    while(argc && **argv == '-') {
        if(strcmp(*argv, "--help") == 0) {
            usage();
            exit(0);
        } else if(strcmp(*argv, "--local") == 0) {
            mode = local;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--gssapi") == 0) {
            mode = gssapi;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--make") == 0) {
            mode = make;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--time") == 0) {
            mode = timing;
            argv++;
            argc--;
        } else if(strncmp(*argv, "--hash", 6) == 0) {
            if((*argv)[6] == '=') {
                digest = *argv + 7;
            } else {
                argv++;
                argc--;
                if(argc == 0) {
                    std::cerr << "token_meister: --hash requires argument" << std::endl;
                    exit(1);
                }
                digest = *argv;
            }

            if(EVP_get_digestbyname(digest.c_str()) == nullptr) {
                std::cerr << "token_meister: invalid hash method: " << digest << std::endl;
                exit(1);
            }

            argv++;
            argc--;
        } else {
            std::cerr << "token_meister: invalid option: " << *argv << std::endl;
            usage();
            exit(1);
        }
    }

    if(argc < 1) {
        usage();
        exit(1);
    }

    std::string private_key = load_private_key(*argv);

    argv++;
    argc--;

    auto fp = get_fingerprint(private_key, digest);

    gethostname(hostname, sizeof hostname);

    switch(mode) {
    case local:
        serve_local(private_key, fp, argc ? *argv : "/run/tdaq_token");
        break;
    case gssapi:
        serve_gssapi(private_key, fp, argc ? *argv : "8991");
        break;
    case make:
        if(argc == 0) {
            std::cerr << "Expected user name" << std::endl;
            exit(1);
        }
        std::cout << make_token(private_key, fp, *argv, hostname);
        break;
    case timing:
        if(argc == 0) {
            std::cerr << "Expected user name" << std::endl;
            exit(1);
        }
        time_it(private_key, fp, *argv);
        break;
    default:
        abort();
    }

    return 0;

}
