
#include "daq_tokens/verify.h"

#include <string>
#include <iostream>

int main(int argc, char *argv[])
{
    bool show_header = false;
    bool show_payload = true;

    std::string input;

    argc--;
    argv++;

    while(argc > 0 && argv[0][0] == '-') {
        if(std::string(argv[0]) == "--help") {
            std::cerr << "verify_daq_token [-q] [-h] [<TOKEN>]\n\n"
                      << "    -h  show header\n"
                      << "    -q  quiet, do not show payload\n";
            exit(0);
        } else if(argv[0][1] == 'h') {
            show_header = true;
        } else if(argv[0][1] == 'q') {
            show_payload = false;
        } else {
            std::cerr << "verify_daq_token: invalid option "
                      << argv[0] << std::endl;
            exit(1);
        }
        argc--;
        argv++;
    }

    if(argc > 0)
        input = argv[0];
    else {
        std::cin >> input;
    }

    try {
        auto token = daq::tokens::verify(input);
        if(show_header)
            std::cout << token.get_header() << std::endl;
        if(show_payload)
            std::cout << token.get_payload() << std::endl;
    } catch(ers::Issue& ex) {
        std::cerr << ex;
        return 1;
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return 1;
    }
}
