// this -*- c++ -*-


#include <boost/asio/ts/net.hpp>

#include <gssapi/gssapi.h>

#include <string>
#include <memory>
#include <exception>


namespace gssapi_utils {

    namespace net = boost::asio;
    using namespace net::ip;
    using boost::system::error_code;

    namespace detail {
        // This is used to free memory received from gssapi.
        // It uses malloc() so to shutup valgrind we
        // have to use free().
        struct deleter {
            void operator()(void *p) { free(p); }
        };
    }

    // Pointer to received data with custom deleter
    using recv_ptr = std::unique_ptr<char[], detail::deleter>;

    class cannot_establish_context : public std::exception
    {
    public:
        cannot_establish_context(const std::string& msg)
            : std::exception(),
              m_message(msg)
        {}

        const char *what() const noexcept override
        {
            return m_message.c_str();
        }
    private:
        std::string m_message;
    };

    /// A helper class for GSSAPI based authentication
    /// and communication.
    ///
    /// This allows a client to authenticate to a server
    /// via e.g. Kerberos and exchange encrypted messages.
    class context {
    public:
        /// Establish a server context
        ///
        /// @param s the socket to communicate with
        /// @service_name  use empty string to use the default credentials
        /// @client_name  out parameter, in 'user@DOMAIN' format
        ///
        /// @throws cannot_establish_context()
        context(tcp::socket&& s, std::string& client_name, const std::string& service_name);

        /// Establish a client context
        ///
        /// @param s the socket to communicate with
        /// @param service_host_name who to connect to in 'service@hostname.domain' format
        ///
        /// @throws cannot_establish_context()
        context(tcp::socket&& s, const std::string& service_host_name);

        ~context();

        /// Send data encrypted to peer
        bool send(const void *buffer, size_t length);

        /// Receive encrypted data from peer
        recv_ptr recv(size_t& length);

        /// Release underlying socket and return (move) it to caller
        /// This is useful if GSSAPI is only used for authentication e.g.
        tcp::socket release();

    private:
        tcp::socket  m_socket;
        gss_ctx_id_t m_context;
    };

}
