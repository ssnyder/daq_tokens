
#include "daq_tokens/acquire.h"

#include <boost/asio.hpp>

#include "gssapi-utils/gssapi.h"

#include <exception>
#include <nlohmann/json.hpp>
#include <jwt-cpp/traits/nlohmann-json/traits.h>
#include <jwt-cpp/jwt.h>

#include "sso-helper/sso-helper.h"

#include <boost/process.hpp>
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <thread>
#include <stdexcept>
#include <mutex>
#include <fstream>

#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>

namespace daq {
    namespace tokens {

        namespace {

            // safety leeway for checks of token expiry = 60 seconds
            const std::chrono::seconds leeway{60};

            // the lifetime of the tokens we generate
            const std::chrono::seconds token_lifetime{1200};

            // for multiple thread synchronization
            std::mutex  mutex;

            // last valid token we acquired and can be re-used
            std::string last_token;

            // expiration time of last_token
            std::chrono::system_clock::time_point last_token_expires_at;

            // last SSO response we got
            nlohmann::json last_sso_token;

            std::string
            get_token_from_environment()
            {
                if(const char *token = getenv("BEARER_TOKEN")) {
                    return token;
                }

                std::string file_name;
                if(const char *filename = getenv("BEARER_TOKEN_FILE")) {
                    file_name = filename;
                } else {
                    uid_t uid = geteuid();
                    if(const char *runtime = getenv("XDG_RUNTIME_DIR")) {
                        file_name = runtime;
                        file_name += "/bt_u";
                    } else {
                        file_name = "/tmp/bt_u";
                    }
                    file_name += std::to_string(uid);
                    file_name += "-atlas-tdaq";
                }

                std::ifstream stream(file_name.c_str());
                std::string token;
                getline(stream, token);
                if(stream.good()) {
                    return token;
                }
                return "";
            }
        }

        std::string
        acquire(Mode mode)
        {
            // only one thread at a time
            std::scoped_lock lock(mutex);

            // The last exception caught.
            // If it is not null when we run out of methods
            // we re-throw it instead to get a hint of what went wrong;
            // otherwise all error information is gone.
            std::exception_ptr eptr = nullptr;

            try {
                // Check if we can re-use the last token
                if(mode == Mode::Reuse) {
                    if(!last_token.empty() && (std::chrono::system_clock::now() < last_token_expires_at - leeway)) {
                        return last_token;
                    }
                }

                std::vector<std::string> methods{"env", "local", "kerberos"};

                if(const char *method_var = getenv("TDAQ_TOKEN_ACQUIRE")) {
                    methods.clear();
                    boost::algorithm::split(methods, method_var,  boost::algorithm::is_space());
                }

                last_token = "";

                for(auto& method : methods) {

                    if(method == "env") {
                        std::string token = get_token_from_environment();
                        if(token.empty()) {
                            continue;
                        }

                        try {
                            auto decoded = jwt::decode<jwt::traits::nlohmann_json>(token);
                            if (decoded.get_expires_at() - leeway < std::chrono::system_clock::now()) {
                                continue;
                            }
                            last_token_expires_at = decoded.get_expires_at();
                        } catch(std::exception& ex) {
                            eptr = std::current_exception();
                            continue;
                        } catch(...) {
                            continue;
                        }

                        last_token = token;

                        return token;
                    }

                    if(method == "local") {

                        // Check if we should use local Unix socket
                        if(const char *path_var = getenv("TDAQ_TOKEN_PATH")) {

                            std::vector<std::string> paths;
                            boost::algorithm::split(paths, path_var, [](char c) { return c == ':'; });
                            std::string buffer;
                            for(auto& path : paths) {

                                try {
                                    boost::asio::local::stream_protocol::iostream sock;
                                    sock.connect(boost::asio::local::stream_protocol::endpoint(path));
                                    sock >> buffer;
                                    sock.close();
                                    // If we got this far and receive nothing, it's an error.
                                    // Don't try to hide it by continuing.
                                    if(buffer.empty()) {
                                        throw std::runtime_error("Received no token from socket");
                                    }
                                    break;
                                } catch (std::exception& ex) {
                                    eptr = std::current_exception();
                                    continue;
                                }
                            }

                            if(buffer.empty()) {
                                // try next method
                                continue;
                            }

                            if(mode == Mode::Reuse) {
                                last_token = buffer;
                                last_token_expires_at = std::chrono::system_clock::now() + token_lifetime;
                            }
                            return buffer;
                        }
                    }

                    if(method == "gssapi") {

                        namespace net = gssapi_utils::net;

                        // default gssapi server host
                        std::string hostname{"vm-atdaq-token.cern.ch"};
                        if(const char *h = getenv("TDAQ_TOKEN_GSSAPI_HOST")) {
                            hostname = h;
                        }

                        // default port
                        std::string port{"8991"};
                        if(const char *p = getenv("TDAQ_TOKEN_GSSAPI_PORT")) {
                            port = p;
                        }

                        net::io_context io_ctx;
                        net::ip::tcp::resolver resolver(io_ctx);
                        auto const results = resolver.resolve(hostname, port, net::ip::tcp::resolver::numeric_service);

                        try {
                            net::ip::tcp::socket client_socket(io_ctx);
                            net::connect(client_socket, results);
                            client_socket.set_option(net::ip::tcp::no_delay(true));

                            gssapi_utils::context ctx{std::move(client_socket), "atdaqjwt@" + hostname};
                            size_t length;
                            auto buffer = ctx.recv(length);
                            if(!buffer) {
                                continue;
                            }

                            std::string result{buffer.get(), length};
                            if (mode == Mode::Reuse) {
                                last_token = result;
                                last_token_expires_at = std::chrono::system_clock::now() + token_lifetime;
                            }
                            return result;
                        } catch (std::exception& ex) {
                            eptr = std::current_exception();
                        }
                        continue;
                    }

                    // SSO based token methods

                    std::string raw_result;

                    // if we have a refresh token, try to use that.
                    if(!last_sso_token.is_null() &&
                       (std::chrono::system_clock::now() > last_token_expires_at - leeway)) {
                        raw_result = daq::sso::get_token_from_refresh_token("atlas-tdaq-token", last_sso_token["refresh_token"]);
                    }

                    if(raw_result.empty()) {

                        if(method == "kerberos") {
                            raw_result = daq::sso::get_token_from_kerberos("atlas-tdaq-token", "ch.cern.atlas.tdaq:/redirect");
                        }

                        if(method == "browser") {
                            raw_result = daq::sso::get_token_from_browser("atlas-tdaq-token");
                        }

                        if(method == "device") {
                            raw_result = daq::sso::get_token_from_device_code("atlas-tdaq-token");
                        }

                        if(method == "password") {
                            // TODO
                        }
                    }

                    if(raw_result.empty()) {
                        // try next method
                        continue;
                    }

                    // Here we  got a new token
                    if(mode == Mode::Reuse) {
                        last_sso_token  = nlohmann::json::parse(raw_result);
                        last_token_expires_at = std::chrono::system_clock::now() + std::chrono::seconds(last_sso_token["expires_in"]);
                        last_token = last_sso_token["access_token"];
                    }
                    return last_sso_token["access_token"];
                }

                // out of methods to try
                if(eptr) {
                    std::rethrow_exception(eptr);
                } else {
                    throw std::runtime_error("No more methods to acquire token");
                }

            } catch (std::exception& ex) {
                throw CannotAcquireToken(ERS_HERE, ex);
            } catch(...) {
                // we don't expect any non-std derived exceptions
                throw;
            }
        }
    }
}
