
#include <string>
#include <iostream>
#include <nlohmann/json.hpp>

#include "sso-helper/sso-helper.h"

int main(int argc, char *argv[])
{
    std::string result = daq::sso::get_token_from_kerberos("sso-helper-test", "http://localhost");
    std::cout << result;

    using nlohmann::json;

    json tokens = json::parse(result);
    std::cout << std::endl << daq::sso::get_token_from_refresh_token("sso-helper-test", tokens["refresh_token"]);
    return (result != "");
}
